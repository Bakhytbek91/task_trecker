from django.urls import path
from accounts.views import (
    LoginView, LogoutView, RegisterView, UserProfileView, UsersListView, UserChangeView, UserPasswordChangeView
)


urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('profile/<int:pk>/', UserProfileView.as_view(), name='profile'),
    path('users/', UsersListView.as_view(), name='users'),
    path('profile/update/<int:pk>/', UserChangeView.as_view(), name='update_profile'),
    path('password_change/<int:pk>/', UserPasswordChangeView.as_view(), name='password_change')
]
