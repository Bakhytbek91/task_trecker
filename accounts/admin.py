from django.contrib.auth import get_user_model
from .models import Profile
from django.contrib.auth.admin import UserAdmin
from django.contrib import admin


class ProfileInline(admin.StackedInline):
    model = Profile
    fields = ['user_description', 'avatar', 'github_link']


class ProfileAdmin(UserAdmin):
    inlines = [ProfileInline]


User = get_user_model()
admin.site.unregister(User)
admin.site.register(User, ProfileAdmin)
