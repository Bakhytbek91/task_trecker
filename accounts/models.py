from django.db import models
from django.contrib.auth import get_user_model


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), related_name='profile',
                                on_delete=models.CASCADE, verbose_name='Пользователь'
                                )
    github_link = models.CharField(max_length=500, null=True, blank=True)
    user_description = models.TextField(max_length=5000, null=True, blank=True)
    avatar = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='Аватар')

    def __str__(self):
        return self.user.get_full_name() + "'s Profile"

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'
