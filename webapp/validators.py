from django.core.exceptions import ValidationError


def at_least_10(string):
   if len(string) < 10:
       raise ValidationError('Данных слишком мало')


def fist_word_upper(string):
    words = list(string)
    if words[0] != words[0].upper():
        raise ValidationError('Первое слово должно начинаться с большой буквы')

