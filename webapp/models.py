from django.db import models
from django.db.models.deletion import get_candidate_relations_to_delete
from django.contrib.auth import get_user_model


class CustomModelManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_deleted=False)


class Types(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return f"{self.name}"


class Statuses(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return f"{self.name}"


class Projects(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    users = models.ManyToManyField(get_user_model(), related_name='projects', blank=True)
    description = models.TextField(max_length=2000, null=False, blank=False)
    start_date = models.DateField(null=False, blank=True)
    end_date = models.DateField(null=True, blank=True)
    is_deleted = models.BooleanField(default=False)

    objects = CustomModelManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        delete_candidates = get_candidate_relations_to_delete(self.__class__._meta)
        if delete_candidates:
            for rel in delete_candidates:
                if rel.on_delete.__name__ == 'CASCADE' and rel.one_to_many and not rel.hidden:
                    for item in getattr(self, rel.related_name).all():
                        item.delete()
        self.save(update_fields=['is_deleted', ])

    def __str__(self):
        return f'{self.name}'


class Tasks(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.SET_DEFAULT, default=1, related_name='tasks', verbose_name='Исполнитель')
    summary = models.CharField(max_length=100, null=False, blank=False)
    description = models.TextField(max_length=2000, null=True, blank=False)
    project = models.ForeignKey('webapp.Projects', on_delete=models.CASCADE, default='1', related_name='project')
    status = models.ForeignKey(Statuses, on_delete=models.PROTECT)
    types = models.ManyToManyField('webapp.Types', related_name='tasks', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Время изменения')
    is_deleted = models.BooleanField(default=False)

    objects = CustomModelManager()

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save(update_fields=['is_deleted', ])

    def __str__(self):
        return f"{self.summary}"
