from django.urls import path
from webapp.views.task_views import (
    TasksView, Task, DeleteTaskView
)
from webapp.views.project_views import (
   ProjectCreateView, ProjectDetailView,
   ProjectUpdateView, ProjectListView, ProjectTaskCreateView, ProjectDeleteView,
   ProjectTaskUpdateView, ProjectUsersView
)

urlpatterns = []

task_urls = [
    path('tasks/', TasksView.as_view(), name='home'),
    path('<int:pk>/', Task.as_view(), name='task_page'),
    path('delete/<int:pk>/', DeleteTaskView.as_view(), name='delete')
]

project_urls = [
    path('', ProjectListView.as_view(), name='projects'),
    path('projects/<int:pk>/', ProjectDetailView.as_view(), name='project_detail'),
    path('projects/create/<int:pk>/', ProjectTaskCreateView.as_view(), name='task_create'),
    path('projects/create/update/<int:pk>/', ProjectTaskUpdateView.as_view(), name='task_update'),
    path('projects/create/', ProjectCreateView.as_view(), name='create_project'),
    path('projects/update/<int:pk>/', ProjectUpdateView.as_view(), name='project_update'),
    path('projects/delete/<int:pk>/', ProjectDeleteView.as_view(), name='project_delete'),
    path('projects/users/<int:pk>/', ProjectUsersView.as_view(), name='project_users')
]

urlpatterns += task_urls
urlpatterns += project_urls
