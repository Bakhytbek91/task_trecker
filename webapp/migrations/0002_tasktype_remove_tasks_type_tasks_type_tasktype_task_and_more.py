# Generated by Django 4.0.1 on 2022-01-22 16:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.RemoveField(
            model_name='tasks',
            name='type',
        ),
        migrations.AddField(
            model_name='tasks',
            name='type',
            field=models.ManyToManyField(blank=True, related_name='tasks', through='webapp.TaskType', to='webapp.Types'),
        ),
        migrations.AddField(
            model_name='tasktype',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='task_types', to='webapp.tasks', verbose_name='Задача'),
        ),
        migrations.AddField(
            model_name='tasktype',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='type_tasks', to='webapp.types', verbose_name='Тип'),
        ),
    ]
