# Generated by Django 4.0.1 on 2022-02-10 14:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0015_tasks_is_deleted'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tasks',
            name='project',
            field=models.ForeignKey(default='1', on_delete=django.db.models.deletion.CASCADE, related_name='project', to='webapp.projects'),
        ),
    ]
