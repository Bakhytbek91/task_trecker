from django import forms
from webapp.models import Tasks, Projects
from .validators import fist_word_upper, at_least_10


class TaskForm(forms.ModelForm):
    summary = forms.CharField(max_length=100, required=True, validators=(fist_word_upper,))
    description = forms.CharField(max_length=2000, required=False, validators=(at_least_10,))
    project = forms.CharField(max_length=100, required=False)

    class Meta:
        model = Tasks
        fields = ['summary', 'description', 'status', 'project', 'types']


class SimpleSearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label="Найти")


class ProjectForm(forms.ModelForm):
    name = forms.CharField(max_length=100, required=True, validators=(fist_word_upper,))
    description = forms.CharField(max_length=2000, required=True, validators=(at_least_10,))
    start_date = forms.DateField(required=True)
    end_date = forms.DateField(required=False)

    class Meta:
        model = Projects
        fields = ['name', 'description', 'start_date', 'end_date']


class TaskFormWithProject(forms.ModelForm):
    summary = forms.CharField(max_length=100, required=True, validators=(fist_word_upper,))
    description = forms.CharField(max_length=2000, required=False, validators=(at_least_10,))

    class Meta:
        model = Tasks
        fields = ['summary', 'description', 'status', 'project', 'types']


class ProjectUsersForm(forms.ModelForm):
    class Meta:
        model = Projects
        fields = ['users']
