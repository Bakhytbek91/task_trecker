from django.contrib import admin
from webapp.models import Types, Statuses, Tasks, Projects


admin.site.register(Types)
admin.site.register(Statuses)
admin.site.register(Tasks)
admin.site.register(Projects)