from django.views.generic import (
    TemplateView, DeleteView
)
from webapp.models import Tasks, Projects
from django.shortcuts import get_object_or_404, reverse
from webapp.forms import SimpleSearchForm
from webapp.helpers import SearchView
from django.contrib.auth.mixins import PermissionRequiredMixin


class TasksView(SearchView):
    template_name = 'tasks/tasks.html'
    context_object_name = 'tasks'
    model = Tasks
    ordering = ['-created_at']
    paginate_by = 10
    search_form = SimpleSearchForm
    search_fields = {
        'summary': 'icontains',
        'description': 'icontains'
    }


class Task(PermissionRequiredMixin, TemplateView):
    template_name = 'tasks/task_page.html'
    permission_required = 'webapp.view_tasks'
    extra_context = None

    def has_permission(self):
        task = get_object_or_404(Tasks, pk=self.kwargs.get('pk'))
        project = get_object_or_404(Projects, pk=task.project_id)
        return super().has_permission() and self.request.user in project.users.all()

    def get_context_data(self, **kwargs):
        task = get_object_or_404(Tasks, pk=kwargs.get('pk'))
        project = get_object_or_404(Projects, pk=task.project_id)
        self.extra_context = {'project': project}
        kwargs['task'] = task
        return super().get_context_data(**kwargs)


class DeleteTaskView(PermissionRequiredMixin, DeleteView):
    template_name = 'tasks/task_page.html'
    model = Tasks
    permission_required = 'webapp.delete_tasks'

    def has_permission(self):
        task = get_object_or_404(Tasks, pk=self.kwargs.get('pk'))
        project = get_object_or_404(Projects, pk=task.project_id)
        return super().has_permission() and self.request.user in project.users.all()

    def get(self, request, *args, **kwargs):
        return self.delete(request=request)

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.get_object().project_id})
