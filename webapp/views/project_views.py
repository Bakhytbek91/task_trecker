from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import reverse, get_object_or_404, render, redirect
from django.views.generic import DetailView, CreateView, UpdateView, ListView, DeleteView

from webapp.forms import ProjectForm, TaskForm, TaskFormWithProject, ProjectUsersForm
from webapp.models import Projects, Tasks
from django.contrib.auth.models import User


class ProjectListView(ListView):
    template_name = 'projects/home_page.html'
    context_object_name = 'projects'
    model = Projects
    ordering = ['name']


class ProjectDetailView(DetailView):
    template_name = 'projects/detail_project.html'
    model = Projects
    project = None

    def get(self, request, *args, **kwargs):
        self.project = get_object_or_404(self.model, pk=kwargs.get('pk'))
        tasks = Tasks.objects.filter(project_id=self.project)
        return render(request, self.template_name, context={'project': self.project, 'tasks': tasks})


class ProjectTaskCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'projects/task_create.html'
    form_class = TaskForm
    model = Tasks
    project = None
    permission_required = 'webapp.add_tasks'

    def has_permission(self):
        project = get_object_or_404(Projects, pk=self.kwargs.get('pk'))
        return super().has_permission() and self.request.user in project.users.all()

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.kwargs.get('pk')})

    def get(self, request, *args, **kwargs):
        project_pk = kwargs.get('pk')
        project = get_object_or_404(Projects, pk=project_pk)
        form = self.form_class()
        return render(request, self.template_name, context={'form': form, 'project': project})

    def post(self, request, *args, **kwargs):
        project_pk = kwargs.get('pk')
        self.project = get_object_or_404(Projects, pk=project_pk)
        form = self.form_class(data=request.POST)
        if form.is_valid():
            types = form.cleaned_data.get('types')
            task = Tasks.objects.create(
                user=self.request.user,
                summary=form.cleaned_data.get('summary'),
                description=form.cleaned_data.get('description'),
                status=form.cleaned_data.get('status'),
                project=Projects.objects.get(pk=project_pk)
            )
            task.types.set(types)
            return redirect(self.get_success_url())
        return render(request, self.template_name, context={'form': form, 'project': self.project})


class ProjectCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'projects/create_project.html'
    form_class = ProjectForm
    model = Projects
    permission_required = 'webapp.add_projects'

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.object.pk})

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            user = self.request.user
            project = Projects.objects.create(
                name=form.cleaned_data.get('name'),
                description=form.cleaned_data.get('description'),
                start_date=form.cleaned_data.get('start_date'),
                end_date=form.cleaned_data.get('end_date'),
            )
            project.users.add(user)
            url = (reverse('project_detail', kwargs={'pk': project.pk}))
            return HttpResponseRedirect(url)
        return render(request, self.template_name, context={'form': form})


class ProjectUpdateView(PermissionRequiredMixin, UpdateView):
    template_name = 'projects/project_update.html'
    form_class = ProjectForm
    model = Projects
    permission_required = 'webapp.change_projects'

    def has_permission(self):
        project = get_object_or_404(Projects, pk=self.kwargs.get('pk'))
        return super().has_permission() and self.request.user in project.users.all()

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.get_object().pk})


class ProjectDeleteView(PermissionRequiredMixin, DeleteView):
    template_name = 'projects/detail_project.html'
    model = Projects
    permission_required = 'webapp.delete_projects'

    def get(self, request, *args, **kwargs):
        return self.delete(request=request)

    def get_success_url(self):
        return reverse('projects')


class ProjectTaskUpdateView(PermissionRequiredMixin, UpdateView):
    model = Tasks
    template_name = 'projects/task_update.html'
    context_object_name = 'task'
    form_class = TaskFormWithProject
    permission_required = 'webapp.change_tasks'

    def has_permission(self):
        task = get_object_or_404(Tasks, pk=self.kwargs.get('pk'))
        project = get_object_or_404(Projects, pk=task.project_id)
        return super().has_permission() and self.request.user in project.users.all()

    def get_success_url(self):
        return reverse('task_page', kwargs={'pk': self.object.pk})


class ProjectUsersView(PermissionRequiredMixin, CreateView):
    template_name = 'projects/project_users.html'
    form_class = ProjectUsersForm
    model = Projects
    permission_required = 'webapp.change_projects'

    def has_permission(self):
        project = get_object_or_404(Projects, pk=self.kwargs.get('pk'))
        return super().has_permission() and self.request.user in project.users.all()

    def get(self, request, *args, **kwargs):
        project = get_object_or_404(Projects, pk=kwargs.get('pk'))
        users = []
        for num in project.users.all():
            users.append(num.pk)
        workers = User.objects.all()
        form = self.form_class()
        return render(request, self.template_name, context={
            'form': form, 'project': project, 'users': users, 'workers': workers
        })

    def post(self, request, *args, **kwargs):
        project = get_object_or_404(Projects, pk=kwargs.get('pk'))
        form = self.form_class(data=request.POST)
        if form.is_valid():
            users = form.cleaned_data.get('users')
            project.users.set(users)
            return redirect(self.get_success_url())
        return render(request, self.template_name, context={'form': form, 'project': project})

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.kwargs.get('pk')})
