from django.urls import path
from api.views import (
    ProjectDetailView, ProjectUpdateView, ProjectDeleteView,
    TaskDetailView, TaskUpdateView, TaskDeleteView
    )


urlpatterns = [
    path('projects/<int:pk>/', ProjectDetailView.as_view(), name='project_api'),
    path('projects/update/<int:pk>/', ProjectUpdateView.as_view(), name='project_api_update'),
    path('projects/delete/<int:pk>/', ProjectDeleteView.as_view(), name='project_api_delete'),
    path('tasks/<int:pk>/', TaskDetailView.as_view(), name='task_api'),
    path('tasks/update/<int:pk>/', TaskUpdateView.as_view(), name='task_api_update'),
    path('tasks/delete/<int:pk>/', TaskDeleteView.as_view(), name='task_api_delete')
]
