import json
from rest_framework.response import Response
from rest_framework.views import APIView
from api.serializers import ProjectsSerializer, TasksSerializer
from webapp.models import Projects, Tasks


class ProjectDetailView(APIView):
    def get(self, request, *args, **kwargs):
        project = Projects.objects.get(pk=kwargs.get('pk'))
        serializer = ProjectsSerializer(project)
        return Response(serializer.data)


class ProjectUpdateView(APIView):
    def put(self, request, *args, **kwargs):
        project = Projects.objects.get(pk=kwargs.get('pk'))
        data = json.loads(self.request.body)
        serializer = ProjectsSerializer(project, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=400)


class ProjectDeleteView(APIView):
    def delete(self, request, *args, **kwargs):
        project = Projects.objects.get(pk=kwargs.get('pk'))
        project.delete()
        return Response(project.pk)


class TaskDetailView(APIView):
    def get(self, request, *args, **kwargs):
        task = Tasks.objects.get(pk=kwargs.get('pk'))
        serializer = TasksSerializer(task)
        return Response(serializer.data)


class TaskUpdateView(APIView):
    def put(self, request, *args, **kwargs):
        task = Tasks.objects.get(pk=kwargs.get('pk'))
        data = json.loads(self.request.body)
        serializer = TasksSerializer(task, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=400)


class TaskDeleteView(APIView):
    def delete(self, request, *args, **kwargs):
        task = Tasks.objects.get(pk=kwargs.get('pk'))
        task.delete()
        return Response(task.pk)
