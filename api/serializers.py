from rest_framework import serializers

from webapp.models import Projects, Tasks


class ProjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ['name', 'users', 'description', 'start_date', 'end_date']
        read_only_fields = ['is_deleted']


class TasksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = ['user', 'summary', 'description', 'project', 'status', 'types']
        read_only_fields = ['is_deleted']
